package main

import (
	"github.com/gorilla/websocket"
	"log"
	"os"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 4096
)

func readPump(done chan struct{}, webconn *websocket.Conn) {
	defer func() {
		close(done)
		webconn.Close()
	}()

	webconn.SetReadLimit(maxMessageSize)
	webconn.SetReadDeadline(time.Now().Add(pongWait))
	webconn.SetPongHandler(func(string) error { webconn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		mt, message, err := webconn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			return
		}
		log.Printf("recv: %s, type: %s\n", message, mt)
	}
}

func writePump(done chan struct{}, interrupt chan os.Signal, webconn *websocket.Conn) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		webconn.Close()
		log.Println("write closed")
	}()

	for {
		select {
		case <-done:
			return
		// case <-interrupt:
		// 	log.Println("interrupt")
		//
		// 	// Cleanly close the connection by sending a close message and then
		// 	// waiting (with timeout) for the server to close the connection.
		// 	err := webconn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		// 	if err != nil {
		// 		log.Println("write close:", err)
		// 		return
		// 	}
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := webconn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		case <-ticker.C:
			webconn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := webconn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
		// select {
		// case <-done:
		// }
	}
}
