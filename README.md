# Websocket Client

Basic go Websocket client. The goal is connect and listen in to the websocket server at http://staticphantom.com:8003.

This library will utilize [Gorilla Websockets](https://github.com/gorilla/websocket).

## Setup

To run the built binary, there are a few steps needed for setting up this client for use. 

### Environment Variables

The following table has the variable name and default values. 

| Name | Description | Default Value | 
| :-- | :-- | --: |
| SERVER_URL | URL or IP the client will connect | `staticphantom.com` |
| SERVER_PORT | Port of the websocket server | `8003` |
| SERVER_PATH | Path extension or page after the host in the address | `ws` |
| INI_FILE | Path to ini file | `websocket_client.ini` | 


