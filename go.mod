module wsclient

go 1.22.4

require (
	github.com/go-ini/ini v1.67.0
	github.com/gorilla/websocket v1.5.1
)

require (
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/net v0.17.0 // indirect
)
