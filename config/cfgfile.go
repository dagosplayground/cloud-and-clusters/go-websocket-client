package config 

import (
	"fmt"
	"github.com/go-ini/ini"
  "os"
  "net/url"
)

var (
	SERVER_URL    = os.Getenv("SERVER_URL")  // server url dns or ip address
	SERVER_PORT   = os.Getenv("SERVER_PORT") // server port if needed
	SERVER_PATH   = os.Getenv("WS")
	INI_FILE      = os.Getenv("INI_FILE")
	SERVER_STRING url.URL
)

func init() {
	if SERVER_URL == "" {
		SERVER_URL = "staticphantom.com"
	}

	if SERVER_PORT == "" {
		SERVER_PORT = "8003"
	}

	if SERVER_PATH == "" {
		SERVER_PATH = "ws"
	}

	if INI_FILE == "" {
		INI_FILE = "websocket_client.ini"
	}

	cfg, err := ini.Load(INI_FILE)
	if err != nil {
		fmt.Printf("Cannot find file, %s. Creating a default file.\n", INI_FILE)
		CreateDefaultIniFile()
		fmt.Println("Exiting")
		os.Exit(1)
	}
	SetGlobalsFromIni(cfg)
	ShowIniFile(cfg)

	// SERVER_STRING = fmt.Sprintf("%s:%s/%s", SERVER_URL, SERVER_PORT, SERVER_PATH)
	host_addr := fmt.Sprintf("%s:%s", SERVER_URL, SERVER_PORT)
	host_path := fmt.Sprintf("/%s", SERVER_PATH)
	SERVER_STRING = url.URL{Scheme: "ws", Host: host_addr, Path: host_path}
}

func CreateDefaultIniFile() {
	createFile := ini.Empty()
	createFile.Section("server").Key("SERVER_URL").SetValue(SERVER_URL)
	createFile.Section("server").Key("SERVER_PORT").SetValue(SERVER_PORT)
	createFile.Section("server").Key("SERVER_PATH").SetValue(SERVER_PATH)

	createFile.SaveTo(INI_FILE)
}

func ShowIniFile(iniFile *ini.File) {
	fmt.Println("Server Section")
	fmt.Println("SERVER_URL: ", iniFile.Section("server").Key("SERVER_URL"))
	fmt.Println("SERVER_PORT: ", iniFile.Section("server").Key("SERVER_PORT"))
	fmt.Println("SERVER_PATH: ", iniFile.Section("server").Key("SERVER_PATH"))
}

func SetGlobalsFromIni(iniFile *ini.File) {
	SERVER_URL = iniFile.Section("server").Key("SERVER_URL").MustString(SERVER_URL)
	SERVER_PORT = iniFile.Section("server").Key("SERVER_PORT").MustString(SERVER_PORT)
	SERVER_PATH = iniFile.Section("server").Key("SERVER_PATH").MustString(SERVER_PATH)
}
