package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"os/signal"
	"wsclient/config"
)

func main() {
	fmt.Println("Server connection string:", config.SERVER_STRING.String())

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	webconn, _, err := websocket.DefaultDialer.Dial(config.SERVER_STRING.String(), nil)
	if err != nil {
		fmt.Println("Can't connect to", config.SERVER_STRING.String())
		os.Exit(1)
	}

	defer webconn.Close()

	done := make(chan struct{})

	go writePump(done, interrupt, webconn)
	go readPump(done, webconn)

	// Set name on login
	webconn.WriteMessage(websocket.TextMessage, []byte("/nick sockbot"))

	// <Program Live Loop>

	for {
		select {
		case <-done:
			log.Println("Done signal hit")
			return
		}
		// <\Program Live Loop>
	}
	fmt.Println("End")
}
